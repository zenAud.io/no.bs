#include <array>
#include <future>
#include <iostream>
#include <optional>
#include <regex>
#include <set>
#include <sstream>
#include <string>
#include <string_view>
#include <thread>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/asio.hpp>
#include <boost/filesystem.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/graph/transitive_closure.hpp>
#include <boost/hana.hpp>
#include <boost/process.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/cereal.hpp>
#include <cereal/types/array.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/bitset.hpp>
#include <cereal/types/boost_variant.hpp>
#include <cereal/types/chrono.hpp>
#include <cereal/types/common.hpp>
#include <cereal/types/complex.hpp>
#include <cereal/types/concepts/pair_associative_container.hpp>
#include <cereal/types/deque.hpp>
#include <cereal/types/forward_list.hpp>
#include <cereal/types/functional.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/queue.hpp>
#include <cereal/types/set.hpp>
#include <cereal/types/stack.hpp>
#include <cereal/types/string.hpp>
#include <cereal/types/tuple.hpp>
#include <cereal/types/unordered_map.hpp>
#include <cereal/types/unordered_set.hpp>
#include <cereal/types/utility.hpp>
#include <cereal/types/valarray.hpp>
#include <cereal/types/vector.hpp>
#include <range/v3/all.hpp>
#include <xxhash.h>

namespace r = ranges;
namespace fs = boost::filesystem;
namespace bp = boost::process;
namespace a = r::action;
namespace v = r::view;

using namespace std;
using namespace boost::filesystem;
using namespace ranges;
using namespace view;
using namespace boost::process;
using namespace boost::asio;

auto project_file_includes(const path& src, const vector<path>& sources) -> vector<path> {
    auto fin = std::fstream(src.string());
    return r::getlines(fin) | v::filter([](auto line) {
               auto s = string_view(line);
               return s.substr(0, 10) == "#include <";
           }) |
           v::transform([](auto s) {
               auto s_r = s | v::reverse;
               auto i = r::find(s_r, '>');
               int end = &*i - &*r::begin(s);
               return s.substr(10, end - 10);
           }) |
           v::filter([&](auto p) { return r::binary_search(sources, p); });
}

auto dir_contents(const path& d) -> iterator_range<recursive_directory_iterator> {
    assert(is_directory(d));
    using rdi = recursive_directory_iterator;
    return {rdi{d}, rdi{}};
}

auto exec(string_view s) {
    auto io = io_service();

    auto o_stdout = future<string>();
    auto o_stderr = future<string>();

    auto c = child(s.data(), std_in.close(), std_out > o_stdout, std_err > o_stderr, io);
    io.run();

    auto ec = c.exit_code();
    return tuple(ec, o_stdout.get(), o_stderr.get());
}

auto is_header_file(const path& f) {
    auto e = f.extension().string();
    return e == ".hpp" || e == ".h";
}

auto is_source_file(const path& f) {
    auto e = f.extension().string();
    return e == ".cpp" || e == ".c" || e == ".mm" || e == ".m";
}

auto source_files(const path& src_dir) -> vector<path> {
    vector<path> files =
        dir_contents(src_dir) | v::remove_if([](const path& f) { return is_directory(f); }) |
        v::filter([](auto&& f) { return is_source_file(f) || is_header_file(f); }) |
        v::transform([=](auto&& f) { return relative(f, src_dir); });

    files |= a::sort | a::unique;

    return files;
}

auto invoke_compile_cmd(const string& cc_cmd, const path& src, const path& obj) {
    stringstream ss;
    ss << cc_cmd << " -o " << obj.string() << " -c " << src.string();
    return ss.str();
}

template <typename Range>
auto link(const string& link_cmd, Range&& objs, const path& exe) {
    stringstream ss;
    ss << link_cmd << " ";
    for (auto&& path : objs) ss << path << " ";
    ss << "-o " << exe;
    return exec(ss.str());
}

using dependency_graph_t = boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS, path>;

auto dependencies(const path& src,
                  const dependency_graph_t& graph,
                  const map<path, int>& path_to_vtx,
                  const map<int, path>& vtx_to_path) {
    auto[b, e] = boost::out_edges(path_to_vtx.at(src), graph);

    auto input_hdrs = vector<path>{};
    r::transform(r::make_iterator_range(b, e), r::back_inserter(input_hdrs),
                 [&](auto&& edge) { return vtx_to_path.at(boost::target(edge, graph)); });
    input_hdrs |= a::sort | a::unique;
    input_hdrs.erase(r::remove(input_hdrs, src), r::end(input_hdrs));

    return input_hdrs;
}

template <class PathRange>
auto cat_contents(const path& src_dir, PathRange&& input_hdrs) {
    auto is = stringstream{};
    for (auto&& hdr : input_hdrs) {
        auto fs = fs::fstream(src_dir / hdr);
        is << fs.rdbuf();
    }
    return is.str();
}

auto dependency_graph(const path& src_dir, const vector<path>& sources) {
    auto graph = dependency_graph_t{};

    auto path_to_vtx = map<path, int>{};
    auto vtx_to_path = map<int, path>{};
    for (auto&& src : sources) {
        auto vtx = boost::add_vertex(graph);
        path_to_vtx[src] = vtx;
        vtx_to_path[vtx] = src;
    }

    auto i_graph = dependency_graph_t{};
    for (auto&& src : sources) {
        auto includes = project_file_includes(src_dir / src, sources);
        for (auto&& header : includes)
            boost::add_edge(path_to_vtx[src], path_to_vtx[header], i_graph);
    }
    auto ci_graph = dependency_graph_t{};
    boost::transitive_closure(i_graph, ci_graph);

    for (auto&& src : sources) {
        auto name = src.stem().string();
        auto mac_name = name;
        if (boost::ends_with(mac_name, "_mac")) mac_name = mac_name.substr(0, mac_name.size() - 4);

        auto includes = dependencies(src, ci_graph, path_to_vtx, vtx_to_path);
        // auto includes = project_file_includes(src_dir / src, sources);
        auto mirror_header = [&](auto f) {
            auto fs = f.stem().string();
            return fs == mac_name || fs == name;
        };

        // we want to ignore links from the {a.cpp, a_mac.mm} -> a.hpp...
        for (auto&& header : includes | v::remove_if(mirror_header))
            boost::add_edge(path_to_vtx[src], path_to_vtx[header], graph);

        // ...and add their inversions: a.hpp -> {a.cpp, a_mac.mm}...
        for (auto&& header : includes | v::filter(mirror_header))
            boost::add_edge(path_to_vtx[header], path_to_vtx[src], graph);

        // ... reasoning: when file b includes a.hpp, it implicitly needs objects a.cpp and
        // a_mac.mm.
    }

    auto graph_closure = dependency_graph_t{};
    boost::transitive_closure(graph, graph_closure);

    return tuple(graph, graph_closure, path_to_vtx, vtx_to_path);
}

template <class PathRange>
auto target_tus(PathRange&& roots,
                const dependency_graph_t& graph,
                const map<path, int>& path_to_vtx,
                const map<int, path>& vtx_to_path) {
    auto tus = vector<path>{};

    for (auto&& root : roots) {
        tus.push_back(root);

        try {
            const auto root_vtx = path_to_vtx.at(root);
            auto [b, e] = boost::out_edges(root_vtx, graph);

            r::transform(
                r::make_iterator_range(b, e), r::back_inserter(tus),
                [&](auto&& edge) -> path { return vtx_to_path.at(boost::target(edge, graph)); });
        } catch (const out_of_range& e) {
            cout << "WARNING: driver " << root.string() << " is not in source dir." << endl;
        }
    }
    tus |= a::remove_if([](auto&& f) { return is_header_file(f); }) | a::sort | a::unique;

    return tus;
}

template <class PathRange>
auto make_obj_dirs(const path& obj_dir, PathRange&& tus) {
    vector<path> dirs = tus | v::transform([](path p) { return p.parent_path(); });
    dirs |= a::sort | a::unique;

    for (auto d : dirs) {
        const auto dir = obj_dir / d;
        if (exists(dir) && !is_directory(dir)) fs::remove(dir);
        if (!exists(dir)) create_directories(dir);
    }
}

namespace term {
    const auto success = "\033[32m";
    const auto error = "\033[1;31m";
    const auto reset = "\033[0m";
}  // namespace term

auto obj_db(const path& db_file) {
    auto db = unordered_map<string, uint64_t>{};
    if (fs::exists(db_file)) {
        auto os = fs::ifstream(db_file);
        auto read = cereal::BinaryInputArchive(os);
        read(db);
    }
    return db;
}

auto obj_db(const path& db_file, const unordered_map<string, uint64_t>& db) {
    auto os = fs::ofstream(db_file);
    auto write = cereal::BinaryOutputArchive(os);
    write(db);
}

auto src_to_obj(const path& obj_dir, path src) {
    return obj_dir / src.replace_extension(".o");
}

auto parse_flag(vector<string>& args, string_view arg, bool value = false) {
    if (auto i = r::find_if(args, [=](auto&& a) { return a == arg; }); i != std::end(args)) {
        args.erase(i);
        return true;
    }
    return value;
}

auto parse(vector<string>& args, string_view arg, string_view def_value = ""sv) {
    auto result = string(def_value);
    if (auto i = r::find_if(args, [=](auto&& a) { return a == arg; });
        i != std::end(args) && i + 1 != std::end(args)) {
        result = *(i + 1);
        args.erase(i, i + 2);
    }
    return result;
}

auto parse_all(vector<string>& args, string_view arg) -> vector<string> {
    auto result = vector<string>{};
    for (;;) {
        if (auto e = parse(args, arg, ""); !r::empty(e))
            result.push_back(e);
        else
            break;
    }
    return result;
}

int main(int argc, const char* argv[]) {
    if (argc == 1) return 0;

    const auto cwd = current_path();

    vector<string> args =
        v::iota(1, argc) | v::transform([=](auto ix) { return string(argv[ix]); });

    auto src_dir = path(parse(args, "--src-dir", cwd.string()));
    cout << "source directory: " << src_dir.string() << endl;

    auto obj_dir = path(parse(args, "--obj-dir", cwd.string()));
    cout << "object directory: " << obj_dir.string() << endl;

    auto exe = obj_dir / parse(args, "--exe", cwd.filename().stem().string());
    cout << "executable: " << exe.string() << endl;

    const auto roots = parse_all(args, "--driver");

    const auto db_file = obj_dir / "nobs.db";
    auto sources = source_files(src_dir);

    auto dep_graph = dependency_graph(src_dir, sources);
    auto& [inc_graph, graph, path_to_vtx, vtx_to_path] = dep_graph;

    const auto rng = [](auto&& p) { return apply(r::make_iterator_range, p); };

    if (parse_flag(args, "--print-dependencies")) {
        cout << "dependencies:" << endl;
        for (auto&& v : rng(boost::vertices(inc_graph))) {
            cout << vtx_to_path[v].string() << ':' << endl;
            for (auto&& e : rng(boost::out_edges(v, inc_graph)))
                cout << "  " << vtx_to_path.at(boost::target(e, inc_graph)).string() << endl;
            cout << endl;
        }
        return 0;
    }

    string cc_args = args | v::drop_while([](auto&& arg) { return arg != "--cc"; }) | v::drop(1) |
                     v::take_while([](auto&& arg) { return arg != "--ld"; }) |
                     v::intersperse(" "s) | a::join;

    string ld_args = args | v::drop_while([](auto&& arg) { return arg != "--ld"; }) | v::drop(1) |
                     v::intersperse(" "s) | a::join;

    auto tus =
        target_tus(roots | v::transform([=](auto&& f) { return relative(path(f), src_dir); }),
                   graph, path_to_vtx, vtx_to_path);

    make_obj_dirs(obj_dir, tus);

    auto db = obj_db(db_file);

    auto command_stream_file = obj_dir / "commands.log";
    if (exists(command_stream_file)) fs::remove(command_stream_file);
    auto command_stream = fs::ofstream(command_stream_file);
    auto error_stream_file = obj_dir / "errors.log";
    if (exists(error_stream_file)) fs::remove(error_stream_file);
    auto error_stream = fs::ofstream(error_stream_file);

    auto print_line = [len = 0](auto i, auto N, auto src, auto color) mutable {
        int progress = round(100. * i / N);
        cout << '\r' << string(len + 20, ' ') << flush;
        cout << '\r' << i << '/' << N << " (" << progress << "%)  " << color << src << term::reset
             << flush;
        len = src.size();
    };

    cout << "\n--" << endl;
    cout << "scanning:" << endl;
    auto workload = v::zip(v::iota(0), tus) | v::transform([&, N = r::size(tus) ](auto&& w) {
                        auto[i, f] = w;
                        print_line(i, N, f.string(), term::reset);
                        const auto& [inc_graph, graph, path_to_vtx, vtx_to_path] = dep_graph;
                        auto input = string();
                        try {
                            const auto deps = dependencies(f, graph, path_to_vtx, vtx_to_path);
                            input = cat_contents(src_dir, v::concat(v::single(f), deps));
                        } catch (const out_of_range& e) {
                            input = cat_contents(src_dir, v::concat(v::single(f)));
                        }
                        return tuple(f, XXH64(r::data(input), r::size(input), 13));
                    }) |
                    v::remove_if([&](auto&& x) {
                        auto && [ f, hash_code ] = x;
                        return hash_code == db[f.string()];
                    }) |
                    v::transform([=](auto&& x) {
                        auto && [ f, hash_code ] = x;
                        return tuple(hash_code, f.string(), src_dir / f, src_to_obj(obj_dir, f));
                    }) |
                    r::to_vector;

    using workload_descr_t = typename decltype(workload)::value_type;

    auto io = io_service();

    const auto max_in_flight = 2 * thread::hardware_concurrency();

    auto in_flight = vector<tuple<workload_descr_t, child, future<string>, future<string>>>{};

    auto failed = vector<string>{};
    auto completed = unordered_set<string>{};

    auto n = 0;
    const auto N = r::size(workload);

    cout << "\n--" << endl;
    cout << "compiling:" << endl;
    while (!r::empty(workload)) {
        auto descr = std::move(r::back(workload));
        workload.pop_back();

        const auto & [ hash_code, src, src_path, obj_path ] = descr;

        if (exists(obj_path)) fs::remove(obj_path);

        auto o_stderr = future<string>();
        auto o_stdout = future<string>();

        const auto cc_cmd = invoke_compile_cmd(cc_args, src_path, obj_path);
        // cout << cc_cmd << endl;

        // FIXME: std_out is not used, but we found that when it was redirected to bp::null as per
        // Boost.Process documentation, the call to o_stderr.get() below simply hung. So for now, we
        // grab it but ignore it below.
        auto cc = child(cc_cmd, std_in.close(), std_out > o_stdout, std_err > o_stderr, io);
        in_flight.emplace_back(std::move(descr), std::move(cc), std::move(o_stdout),
                               std::move(o_stderr));

        while (r::size(in_flight) >= max_in_flight || r::empty(workload) && !r::empty(in_flight)) {
            io.run_one();

            if (auto i = r::find_if(in_flight, [](auto&& f) { return !get<child>(f).running(); });
                i != r::end(in_flight)) {
                auto item = std::move(*i);

                auto & [ descr, cc, o_stdout, o_stderr ] = item;
                const auto & [ hash_code, src, src_path, obj_path ] = descr;

                const auto ec = cc.exit_code();
                const auto err_str = o_stderr.get();

                completed.insert(src);
                n = r::size(completed);

                const auto cc_cmd = invoke_compile_cmd(cc_args, src_path, obj_path);
                command_stream << cc_cmd << endl;

                if (exists(obj_path)) {
                    db[src] = hash_code;
                    obj_db(db_file, db);
                    print_line(n, N, src, term::success);
                } else {
                    print_line(n, N, src, term::error);
                    error_stream << err_str << endl;
                    failed.emplace_back(src);
                }
                in_flight.erase(i);
            }
        }
    }

    if (!r::empty(failed)) {
        cout << "\n\nfailed:\n"
             << term::error
             << (failed | v::transform([](auto&& f) { return f + "\n"; }) | a::join)
             << term::reset << endl;
    } else {
        if (exists(exe)) fs::remove(exe);
        cout << "\n--" << endl;
        cout << "linking:" << endl;
        auto [code, out, err] = link(
            ld_args, tus | v::transform([=](auto&& f) { return src_to_obj(obj_dir, f); }), exe);
        if (exists(exe))
            cout << "link: success. " << out << "; " << err << endl;
        else {
            cout << "link: fail. " << endl;
            error_stream << err << endl;
        }
    }

    return 0;
}
